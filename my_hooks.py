def on_page_markdown(markdown, page, config, files, **kwargs):
    extra = ["\n\n## Test\n\n"]
    for f in files:
        if f.src_path.startswith("categories"):
            extra.append("* " + f.src_path)

    return markdown + "\n".join(extra)


