> “Solutions rarely arise except by contact with & sensual experience of the material.”
> 
> – Anthony Froshaug, Typographic norms (1964)

Le printlab est un atelier centré sur l'expérimentation des techniques numériques d'impression. C'est un espace ouvert à l'ensemble des étudiant·e·s et professeur·e·s de l'[ÉSA Le 75](https://leseptantecinq.be) mettant à disposition une série de [machines pour l'impression et le façonnage](materiel). Le lieu accueille également certains cours liés à l'images numérique.

Ce wiki regroupe un ensemble de [ressources](), [tutoriels](), [briefs](), [workshops]() ou de [cours]() ou documentation de projets.

Les codes, bribes et fragments produits pour les cours d'Image numérique ou du Printlab sont disponible sur notre [Gitlab](https://gitlab.com/le75).

[En savoir plus sur le projet](./a_propos).
