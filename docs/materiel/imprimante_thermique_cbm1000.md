---
categories:
    - Materiel
---

# Imprimante thermique CBM1000

![](/img/thermal-printer-cb-1000.jpg)


## Spécifications 

* 72 mm/576 dots, (54 mm/432 dots)

## ressources 

* [Manuel d'utlisation](/img/i8x-cbm1000usermanual.pdf )
