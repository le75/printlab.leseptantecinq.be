---
categories:
    - Materiel
---

# Plotter HP 7550a

![](/img/hp7550a.png)

increment minimal
:   0.025mm

résolution maximale
:   1016 dpi

force
:   entre 1 et 8

vélocité
:   entre 10 et 80

## A4

Surface utile
:   191mm x 272mm

en unités plotter
:   7640 x 10880

## A3

Surface utile
:   272mm x 399mm

en unités plotter
:   10880 x 15960


## Ressources 

* [Manuel](/img/hpgraphicsplotters_colorpro-7475a-7550a_technicaldata_5954-8797_12pages_jun87.pdf)
