---
categories:
    - Materiel
---

# Riso MZ 970

## Tambours et encres

7 tambours disponibles (mais 5 étuis seulement):

* noir
* bleu
* rose fluo
* orange fluo
* jaune
* vert
* marron (doré)

3 type de pigments:

* opaque (marron/doré)
* fluo (rose et orange)
* translucide (les autres)

Format maximal: a3+

NOTE:
Les réserves de pigments/cartouches et film «master» se trouvent dans l'armoire numéro 3. Il n'y a plus en réserve ? Signalez-le pour en recommander.

## Utilisation

### Mise en route

1. mettre sous tension la machine (intérupteur à droite) ou la sortir de veille (bouton vert avec une ampoule sur le panneau de commandes)

2. Relever le nombre de copie

3. Charger les tambours: ouvrir le panneau avant et désenclencher la sécurité en appuyant sur le bouton vert situé entre les deux tambours. Sortir les tambours et les remplacer puis enfoncer les tambours.
    * 1 couleur: charger le tambour dans l'emplacement de gauche
    * 2 couleurs: charger la plus clair à gauche, la plus foncée à droite. Sauf pour les fluos: charger le fluo à droite.

4. Vider les récuperateurs de master usés si nécéssaire (un à droite, un à gauche). Nettoyer si nécéssaire avec un chiffon et un peu d'alcool les contours de la machines, roulements et autres (mais pas sur la vitre de la machine)

5. Charger quelques feuilles de papier (feuilles de passe dans un premier temps) du même format. Idéalement du même grammage. Vous pouvez récupérer des feuilles de passe dans le bac à recyclage à gauche de la machine. Quand on créé un master, il y a toujorus au minimum deux copies imprimées (pour encrer uniformément les feuilles). Il peut être nécéssaire d'en passer jusquà 10 pour avoir une impression vraiement uniforme.

6. Si le bac d'alimentation (situé à gauche de la machine) n'est pas descendu, appuyer sur le bouton vert juste au dessus du bac. Régler les taquets.

## Pour une impression deux couleurs à partir de la vitre (scanner) de la machine

1. placer l'original sur la vitre. Attention: le format doit être centré verticalememnt
2. appuyer sur le bouton tout à gauche du panneau de commande. Choisir 2 couleurs
3. effectuer les réglages de contraste/format et autres sur l'écran
4. idem pour la couleur 2 (recommencer à 1.)

## Pour imprimer depuis le PC

NOTE:
C'est d'abord la droite puis la gauche.

1. allumer l'ordi + le rip. Le rip doit uniquement avoir sa led verte allumée (ready). Attendre quelques minutes si ce n'est pas le cas. L'ordi a une pile usée. mais en attendant, appuyer sur F1 pour lancer le démarrage

2. Booter sur windows XP

3. Charger votre fichier au format `PDF` sur une clé

4. Ouvrir le fichier avec le lecteur PDF (`PDFXchange viewer`)

5. Menu `fichier > Imprimer`.

6. Choisir l'imprimante/RIP `RISO PS7R-MZ970` (Si impression d'une grande image qui laisse des trait blancs, cocher «imprimer comme une image»

7. Aller dans `propriétés`:
    1. Panneau `paramètres principaux`, `marche automatique = ON`
    2. Panneau coloration: choisir `Impresion monochrome` puis la couleur cible (d'abord fair le tambour de droite si impression deux couleurs) Laisser `tambour` sur «auto»
    3. dans le panneau `traitement de l'image`, laisser `diffusion d'erreur` sauf si vous voulez une trame mecanique à un angle précis (en cas d'impression quadri par exemple)
    4. dans le panneau `mise en page`, changer le zoom ou forcer «100%», et eventuellement l'orientation

8. Refaire la même chose pour le tambour de gauche.

9. Relever le nombre de copie
