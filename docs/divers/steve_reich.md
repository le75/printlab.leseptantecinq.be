## Steve Reich 

Musicien Américain (1936-). Un des inventeur du minimalisme, et de principes de compositions tels que le phasage/déphasage.

{{youtube>BhhIZscEE_g}}

{{youtube>lzkOFJMI5i8}}

{{youtube>eu-tRXgOrdg}}

{{youtube>RTke1tQztp}}

{{youtube>1E4Bjt_zVJc}}

{{youtube>uDhwFTw4VnI}}

{{youtube>PsKP_dpNlxg}}

