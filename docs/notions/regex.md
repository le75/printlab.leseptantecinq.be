---
categories:
    - Notion
---

# Expressions rationelles

Derrière le nom barbare d\'expressions rationnelles se cache la
possibilité de recherche/remplacer par motif. rechercher par motif
signifie rechercher non pas un terme précis, mais une structure de
texte.

## Un exemple {#un_exemple}

En anglais, le mot *man* (homme) a pour pluriel *men*. En utilisant un
logiciel supportant les expressions rationnelles, vous pouvez rechercher
toutes les occurrences de *man* et *men* en une seule fois, à l\'aide de
ce motif:

`   m[ea]n`

Les crochets sont utilisés pour définir une liste de caractères
possibles. Ce motif signifie donc littéralement *recherche la lettre m,
suivie de la lettre a ou e, suivie de la lettre n*. Donc soit *man* soit
*men*. On peut pousser le bouchon plus loin:

`   (wo)?m[ea]n`

ce qu\'on remarque, c\'est que certains caractères ont des
significations spéciales (parenthèses, crochets, point
d\'interrogation). On peut alors les préfixer d\'un antislash (\"\\\")
pour les signifiers comme caractère à chercher.

## En quoi est-ce utile ? {#en_quoi_est_ce_utile}

-   De manière pratique, nettoyer un texte et corriger certaines erreurs
    typo ;
-   appliquer des styles à certain types de contenus ;
-   rechercher des contenus ;
-   extraire du sens analysant des motifs récurrents d\'un corpus de
    textes.

## Exemples artistiques {#exemples_artistiques}

-   [gatt.org](http://www.gatt.org), parody des Yes Men
-   [videogrep](http://activearchives.org/wiki/Videogrep) (data-cinéma)

`   ~/bin/videogrep.sh Tampopo.cd1.avi noodles`

## Egrep

\`grep\`, \`egrep\`, \`fgrep\`, \`rgrep\` - Afficher les lignes
correspondant à un motif donné.

Il existe plusieurs versions du programme \`grep\`: la version \"GNU\"
et la version \"BSD\". Les programmes fonctionnent de manière
quasi-identique, à savoir:

`   grep "motif" fichier.txt`

La commande ci-dessus affiche toutes les lignes du fichier nommé
\"fichier.txt\" qui comporte le texte \"motif\". Plutôt que de chercher
un mot défini, on peut décrire un motif, c\'est à dire une structure
verbale, par exemple:

`   egrep m[ae]n fichier.txt`

On utilise ici le programme \"egrep\" plutôt que \"grep\" car il permet
d\'utiliser les expressions rationnelles avancées. La commande ci-dessus
trouvera les mots \"man\" et \"men\". Elle trouvera également les mots
\"woman\", \"commande\", \"manipuler\", \"menacer\" etc. car les motifs
\"man\" et \"men\" sont présent dans ces mots. Si on souhaite si limiter
aux mots \"man\" et \"men\", il faut être un peu plus précis, et écrire:

`   grep "\bm[ae]n\b" fichier.txt`

La commande ci-dessus introduit une nouvelle précision: \`\\b\` signifie
\"limite de mot\". En placant ce symbole avant et après notre motif
\`m\[ae\]n\` on décrit que littéralement: \"limite de mot; lettre m;
lettre a ou e; lettre n; limite de mot\". Notre recherche est donc
suffisement précise pour n\'afficher que les mots \"man\" ou \"men\".

### D\'autres exemples {#dautres_exemples}

imprimer toutes les lignes avec des chiffres

`   egrep "\d" fichier.txt`

imprimer toutes les lignes avec des nombres à quatre décimales

`   egrep "\d\d\d\d" fichier.txt`

imprimer toutes les lignes avec des nombres à deux, tris ou quatre
décimales

`   egrep "\d{2,4}" fichier.txt`

imprimer toutes les lignes avec des nombre à quatre décimale ou plus

`   egrep "\d{4,}" fichier.txt`

imprimer toutes les lignes avec des mots finissant par \"ing\"

`   egrep "ing\b" fichier.txt`

imprimer toutes les lignes avec le texte \"man\" ou \"woman\"

`   grep "(wo)?man"`

Toutes les phrases qui commencent par \"When\"

`   grep "^When" obama-2008.txt`

### Pour aller plus loin {#pour_aller_plus_loin}

Plutôt que de lister ici toutes les possibilités offertes par les
expressions rationnelles, veuillez plutôt vous référer à cette [page
d\'aide](http://www.cheatography.com/davechild/cheat-sheets/regular-expressions/)
(en anglais)

Vous pouvez également jeter un coup d'œil à la page [10 choses à faire
avec la ligne de
commande](10_choses_à_faire_avec_la_ligne_de_commande "wikilink").
