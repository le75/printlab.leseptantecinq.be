---
categories:
    - Notion
---

# Image Bitmap

## Terminologie

Bit
:    Plus petite unité d'information stockable et manipulable par un ordinateur binaire. Représenté généralement par le couple 1/0 ou vrai/faux (base 2)

pixel
:    Le plus petit élément d'une image matricielle.

point
:    identique au pixel. Le terme est préféré lorsqu'on parle d'une image imprimée.

pouce
:    unité de longueur équivalente à 2,54 cm;

Définition d'une image
:    Le nombre de pixels que comporte une image. Si une image fait 800 px de large et 600 px de haut, alors sa définition est 800 × 600, soit 480 000 px ou 0,48 mégapixel.

Résolution d'une image
:    le nombre points/pixels qui peuvent rentre dans une longueur donnée, généralement exprimée en pouce. Ex: 72 dpi («dot per inch» soit «point par pouce» en français)

Profondeur d'une image
:    Quantité d'information stockée pour un pixel


## Définition

![](/img/axel_korban_40x30.png "Image de 40 × 30 px. Axel Korban, CC BY-SA")


## Profondeur

![](/img/axel_korban_dsc6993_24bits.jpg "Image RVB 24 bits par pixel, soit 3 × 8 bits. Axel Korban, CC BY-SA")

![](/img/axel_korban_dsc6993_8bits.jpg "Image Gris 8 bit par pixel. Axel Korban, CC BY-SA")

![](/img/axel_korban_dsc6993_1bit.png "Image NB 1 bit par pixel. Axel Korban, CC BY-SA")


## Les bases en maths

![Gottfried Wilhelm Leibniz, 1697](/img/liebniz_binary.jpg "Gottfried Wilhelm Leibniz, 1697")


## Résolution d'une image


Quelle nombre pixel faut t'il pour créer une image de 156 mm x 106 mm en 600 ppp (ou 23,7 point par mm)?

156 * 23,7 = 3697 px
106 * 23,7 = 2512 px
