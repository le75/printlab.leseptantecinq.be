---
categories:
    - Notion
---

# 10 choses à faire avec la ligne de commande

## Introduce yourself


```bash
echo I am `whoami`. It is `date` and we are on $HOST, in the directory `pwd` | espeak
```

## What time is it in Sao Paulo at 18:47 in Brussels?

```bash
TZ=America/Sao_Paulo date "+%F %R (%Z%z)" -d 'TZ="Europe/Brussels" 18:47'
```

## Get the alphabetical list of words in a text

```bash
cat text.txt | tr " " "\n" | sort | uniq
```

## Do a cadavre exquis of several texts

```bash
cat a.txt b.txt c.txt > abc.txt
```

## Make a top 5 of your fattest files

```bash
ls -l -S *.* | head -5
```

## Create an animated gif of system icons

```
convert /usr/share/pixmaps/*.{png,xpm} /tmp/animated.gif
```

## Make a randomized playlist of a sound sytem and listen relax

```bash
find /usr/share/sounds/ -type f | shuf > /tmp/playlist.list && mplayer -playlist /tmp/playlist.list
```

## Make a parody of Wikipedia

```bash
wget -k -O /tmp/o.html `[`https://en.wikipedia.org/wiki/BNP_Paribas`](https://en.wikipedia.org/wiki/BNP_Paribas)` && sed -i "s/\w\+ing/schtroumpfing/gi" /tmp/o.html
```
    
## Make a poem out of your computer memory

```bash
sudo dd if=/dev/mem 2> /dev/null | cat | strings | shuf | head
```

## Rank the commands by popularity

```bash
history | awk '{print $2}' | sort | uniq -c | sort -rn | head
```
