# À propos

Par Christophe Alix, directeur de l'ÉSA Le Septantecinq

S’ouvre enfin le Printlab. Voici une inauguration à la lumière de ce que sera ce nouveau laboratoire initié par les professeurs de l’atelier d’Images Plurielles à l’ESA LE 75 : une lente immersion dans un espace en construction, en réseau et alternatif.

Le Printlab est aujourd’hui, et pour les jours qui suivent, officialisé comme une entrée sans matière, le début d’un projet global sans projet particulier, la volonté réelle d’initier une réflexion sur les images imprimées sans forcément produire une œuvre.

Le Printlab va plus loin que l’idée de projeter de la matière ou projeter de l’œuvre, il propose déjà de découvrir ce que l’on connaît des machines (celles que l’on a autour de nous) et,comment nous pouvons leur donner d’autres tâches à accomplir que celles pour lesquelles elles sont manufacturées ou programmées.

Le Printlab est inspiré du Fablab et du Hackerspace, ces espaces de collaboration entre communautés du monde entier réunis autour d’intérêts spécifiques liés pour le premier à donner accès à des outils de fabrication industrielle et pour le second à partager des données autour de la technologie. Leur but premier est dans les deux cas de déjouer les mécanismes conventionnels et commerciaux de l’utilisation des machines, réseaux et ressources technologiques.

Le Printlab favorise l'appropriation des technologies d’impression plus que de donner accès à du matériel dernier cri. Un nombre impressionnant de matériels technologiques et analogiques s’accumule dans nos caves, placards, au-dessus de nos armoires, sous la table,aux puces, etc. Les centres de recyclage débordent de ces vieilles machines qui ont pour particularité de fonctionner encore mais n’ont plus aucune utilité manifeste pour une majorité de gens. La technologie est en constante évolution, les changements technologiques s’opèrent à une vitesse fulgurante et il nous reste tout ce vieux matériel qui périt là, sans même pouvoir pourrir. C’est ainsi que la “matière première” du Printlab comportera de vieilles imprimantes, photocopieuses, risographes, plotters, etc. On partira donc ici de la récup pour construire d’autres images.

Le Printlab est un laboratoire d’expérimentation dans lequel on apprend à démonter, détourner, bricoler et imaginer des utilisations non convenues pour ce matériel.

Le Printlab est une plateforme de réflexion et de partage. Les technologies open-source seront privilégiées pour permettre l’interopérabilité des différentes machines et ouvrir l’espace à des pratiques extérieures (workshops, échanges d’informations via le web, projets communs avec d’autres structures). Il s’agit de construire des outils et expériences autour de collaborations,échanges et apprentissages collectifs au sein du Printlab. Le site web du Printlab sera aussi un lieu de documentation et d’échanges.Les membres du Printlab y auront chacun un espace personnel et pourront y documenter leurs projets ou consulter d’autres projets à tout moment.

Le Printlab privilégie les approches DIY (« do it yourself »), une forme d’artisanat favorisant un apprentissage par la pratique,et revendique les vertus pédagogiques de la bidouille. David Cuartielles de chez Arduino disait : “Si vous voulez apprendre l’électronique, vous devriez pouvoir apprendre par la pratique dès le premier jour, au lieu de commencer par apprendre l’algèbre”.La pratique demande d’assumer sa position d’observateur et d’apprenant, de participant et de constructeur, de technicien et d’artiste, d’ingénieur et d’usager, de développeur et d’utilisateur, toutes ces frontières fonctionnelles étant ici appelées à disparaître.

Le Printlab cherchera à utiliser toute sorte de support d’impression et pourra certainement s’inspirer à plus grande échelle de tous les acquis techniques et artistiques de la sérigraphie, de la gravure et du graphisme. Nous poserons ici la question de la matérialisation de l’espace virtuel en espace physique alors que l’inverse va aussi de soi.

Le Printlab ne se résume pas à une seule chose ou un seul objectif. Il émerge cependant à partir d’un esprit commun, celui de prendre le contre-pied de ce qui existe de manière habituelle, de ce qui devient trop banal dans l’utilisation et l’application de la machine. La machine est une chose à dompter, elle ne domine que ceux et celles qui se laissent aller à l’utilisation de la technologie sans poser la moindre réflexion à l’outil, à moins que nous traitions de la question autrement en supposant que l’homme est conduit à dialoguer et jouer avec la machine, c’est à discuter.

Quoi qu’il en soit, l’ouvrière de l’art est une manipulatrice de matières, elle ira bidouiller l’intérieur des choses pour comprendre son fonctionnement, leur faire accomplir l’impossible pour devenir, à travers de multiples mutations, cette nouvelle chose qui pourra enfin revendiquer une identité unique.

Christophe Alix (*à partir de la Charte du Printlab)
