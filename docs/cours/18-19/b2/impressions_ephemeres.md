---
categories:
    - brief 
---

# Impressions éphemères

## description 

### Première étape 

Extraire les 15 dernières images de son smartphone. Faire une sélection d'images et dégager un thème (sujet, aspect plastique etc.). Éventuellement compléter la sélection avec d'autres images. Trouver un traitement graphique en adéquation avec son thème, et avec la contrainte de la trame bitmap noir/blanc. Imprimer ces images à l'aide de l'imprimante thermique.


### Deuxième étape 

Sélection tout ou partie de ces impressions, et effectuer une transformation (agrandissement, recadrage, superposition, etc.) et sérigraphie sur le support textile de son choix.

### Troisième étape 

Réaliser un plié proposant une lecture en livret ainsi qu'une lecture dépliée (poster).


## thème 

«Je me souviens»

## critères d’évaluation 

* respect des briefs et bonne gestion du temps
* qualité et quantité du travail fourni
* engagement personnel dans les activités.
* maîtrise et compréhension des techniques
* adéquation des choix techniques et plastiques
* cohérence des images et du propos développé
* capacité de remise en question
* qualité de la réflexion sur son travail (documentation, discussions)

## Remise 

Le mardi 7 mai 2019
