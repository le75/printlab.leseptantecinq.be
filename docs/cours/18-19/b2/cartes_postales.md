---
categories:
    - brief 
---

# Cartes postales

## description 

Réinterpréter une série de 5 cartes postales à l’aide de trames hachurées.

Vous utiliserez pour ça le logiciel Inkscape et l'extension Eggbot.

Possibilité de varier les distances et les angles. 2 couleurs maximum. Impression au plotter.

En lien avec le travail de boites de Muriel et celui de réinterprétation de Roberta.

## thème 

«Je me souviens»

## critères d’évaluation 

* respect des briefs et bonne gestion du temps
* qualité et quantité du travail fourni
* engagement personnel dans les activités.
* maîtrise et compréhension des techniques
* adéquation des choix techniques et plastiques
* cohérence des images et du propos développé
* capacité de remise en question
* qualité de la réflexion sur son travail (documentation, discussions)

## Remise 

Le jeudi 06 décembre 2018
