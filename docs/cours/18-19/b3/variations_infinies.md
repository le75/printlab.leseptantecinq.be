---
categories:
    - brief 
---

# Variations infinies

## description 

Créer 100 variations d’une même image générative.

Votre série d’image doit comporter un principe de construction clair et formalisable, utilisant les notions de répétition, paramètres et variables.

Le logiciel Processing est utilisé en cours pour comprendre ces notions de bases, mais son utilisation n’est pas obligatoire.

Un fabrication «main», plutôt qu’informatique est envisageable à condition de pouvoir présenter clairement un processus formel (à discuter au cas par cas, en concertation).

Liens et croisements possibles avec le travail Grand tirage de Fred Deltenre.

## thème 

libre.

## critères d’évaluation 

* respect des briefs et bonne gestion du temps
* qualité et quantité du travail fourni
* engagement personnel dans les activités.
* maîtrise et compréhension des techniques
* adéquation des choix techniques et plastiques
* cohérence des images et du propos développé
* capacité de remise en question
* qualité de la réflexion sur son travail (documentation, discussions)

## Remise 

Le vendredi 14 décembre 2018
