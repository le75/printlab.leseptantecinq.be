---
categories:
    - brief
---

# Fragments

![Photo d'Axel Korban](/img/imagemagick/axel.png)

Photographie d'Axel Korban, tirée du magazine [Médor Numéro 8](https://medor.coop/magazines/medor-8-autumn-2017), CC BY-SA

Ce travail vise à déconstruire un ensemble d’images à travers un processus de sélection, de fragmentation et de changement de medium pour en reconstruite une lecture nouvelle.

### description 

Sélectionnez un jeu de photos à partir de votre téléphone (environs 5 à 7 images). Ces images peuvent être liées par un thème, un lieu, une analogie formelle, etc.

Vous imprimerez ensuite, sur papier thermique, tout ou portion de celle-ci, après avoir traité ces images en noir ou blanc en expérimentant différentes trames (diffusion, géométriques, etc.).

À partir de ces impressions, vous sélectionnerez des fragments auxquels vous appliquerez un changement d’échelle, en jouant sur et frontière entre lisibilité et abstraction de la trame et du sujet, pour ensuite les réinterpréter sur textile (sérigraphie, teinture ou autre procédé à expérimenter ensemble).

Enfin, vous produirez un plié qui viendra rendre compte de votre processus et, telle une mosaïque, donnera une lecture nouvelle de vos images d’origine en composant avec les images obtenues lors de différentes étapes.

### thème 

“Dualité” (à travers la trame noir/blanc)

### critères d’évaluation 

  * respect des briefs et bonne gestion du temps
  * qualité et quantité du travail fourni
  * engagement personnel dans les activités.
  * maîtrise et compréhension des techniques
  * adéquation des choix techniques et plastiques
  * cohérence des images et du propos développé
  * capacité de remise en question
  * qualité de la réflexion sur son travail (documentation, discussions)

### Remise 

Le mardi 12 mai 2019

### Ressources 

  * [Libcaca: Error diffusion](http://caca.zoy.org/study/part3.html)
  * [Image dithering](https://pierremuth.wordpress.com/tag/error-diffusion/)
  * [Joel Yliluoma's arbitrary-palette positional dithering algorithm](http://bisqwit.iki.fi/story/howto/dither/jy/)
  * [Image Dithering: Eleven Algorithms and Source Code](https://www.tannerhelland.com/4660/dithering-eleven-algorithms-source-code/)
  * [Error Diffusion Dithering in R](https://coolbutuseless.bitbucket.io/2018/02/04/error-diffusion-dithering-in-r/)
  * [Dithermark](https://app.dithermark.com/)
