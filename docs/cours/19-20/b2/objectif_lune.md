---
categories:
    - brief
---

# Objectif lune

![Réinterpréatation d'une photo de la lune au plotter](/img/moon.plt.png)

## description 

Sélectionner une série cohérente de 5 images photographique autour de la conquête lunaire.

Les réinterpréter à l'aide des différentes techniques de tramage abordées en cours (trames horizontales, croisées, circulaires, etc.).

Impression au Plotter suivi d'un deuxième passage avec la technique de votre choix (dessin, risographie, sérigraphie, gravure, etc.) afin d'en modifier le sens.

Pensez à exploiter les différents paramètres disponibles (par exemple: valeurs de temps d'appui, de pression d'appui et d'absorption du papier).

Le format est libre, mais devra être cohérent avec le travail de Roberta et Muriel: pensez l'objet final dans son ensemble!

En lien avec le travail de boites de Muriel et celui de réinterprétation de Roberta.

## thème 

«Dualité»

## critères d’évaluation 

  * respect du brief et bonne gestion du temps
  * qualité et quantité du travail fourni
  * engagement personnel dans les activités.
  * maîtrise et compréhension des techniques
  * adéquation des choix techniques et plastiques
  * cohérence des images et du propos développé
  * capacité de remise en question
  * qualité de la réflexion sur son travail (documentation, discussions)

## Remise 

Le vendredi 06 décembre 2018

## Liens 

  * [Releran 2014 Cirles](https://gitlab.com/relearn/relearn2014/tree/master/circles)
  * [Relearn 2014 patterns](http://osp.constantvzw.org:9999/p/relearn-2014-patterns)
  * [Pyplot](https://gitlab.constantvzw.org/osp/tools.pyplot)
  * [Images de la Nasa](https://images.nasa.gov/)
