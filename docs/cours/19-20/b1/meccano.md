---
authors: 
    - Alex Leray
    - Ronan Deriez
categories:
    - workshop
---

# Meccano

![Karel Martens' monotype](/img/karel_martens_monotype.jpg "Karel Martens Sans titre, circa 1991 monotype sur carte pré-imprimée")

Cours commun avec Ronan Deriez (TT) et Alexandre Leray (atelier).

## description

Découverte de la chaîne graphique d'impression Risographique.

À l'aide des pièces standard Meccano, des copieurs xerox et du [Risographe](/materiel/Riso Risograph GR3750), créer une composition de motifs répétitifs.

Il vous sera demandé de tester les superpositions de couleurs afin d'en créer de nouvelles par association.

Dans un premier temps, travailler à l'échelle 1 pour créer les motifs de base.

Dans un deuxième temps, changer l'échelle pour créer une composition.

{{:undefined:img_20200205_144735.jpg?400|}}


## thème

Abstraction, libre. 

## critères d’évaluation

* respect du brief et bonne gestion du temps
* qualité et quantité du travail fourni
* engagement personnel dans les activités.
* maîtrise et compréhension des techniques
* adéquation des choix techniques et plastiques
* cohérence des images et du propos développé
* capacité de remise en question
* qualité de la réflexion sur son travail (documentation, discussions)

## Remise

Le mercredi 05 décembre 2018

## Liens

* [Portfolio de Gianpaolo Pagni](http://www.gianpaolopagni.com/)


## Projets

### Ilona 

{{:cours:img_5015.jpg?400|}}
{{:cours:img_5014.jpg?400|}}
{{:cours:img_5024.jpg?400|}}
{{:cours:img_5025.jpg?400|}}
{{:cours:img_5027.jpg?400|}}
{{:cours:img_5036.jpg?400|}}
{{:cours:img_5039.jpg?400|}}

### Louise de Cartier 

{{:cours:img_20200204_140029.jpg?400|}}
{{:cours:img_20200205_142117.jpg?400|}}
{{:cours:img_20200205_142206.jpg?400|}}

### Maxime 

{{:cours:img_0180.jpg?400|}}
{{:cours:img_0183.jpg?400|}}
{{:cours:img_0188.jpg?400|}}
{{:cours:img_0186.jpg?400|}}
{{:cours:img_0187.jpg?400|}}
{{:cours:img_0191.jpg?400|}}

### Lucie 

{{:cours:20200205_140151.jpg?400|}}
{{:cours:20200205_140046.jpg?400|}}

### Maëlle 

{{:cours:img_5042.jpg?400|}}
{{:cours:img_5046.jpg?400|}}
{{:cours:img_5048.jpg?400|}}
{{:cours:img_5044.jpg?400|}}
{{:cours:img_5052.jpg?400|}}
