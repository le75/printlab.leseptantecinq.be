# Introduction à la typographie

> “Solutions rarely arise except by contact with & sensual experience of the material.”
> 
> – Anthony Froshaug, Typographic norms (1964)

## Typographie 

### définition 

Typographie != lettrage

Lettrage: lettres dessinées.

Typographie: lettres imprimées.

{{:cours:metal_movable_type.jpg?400|}}

''Willi Heidelbach (https://commons.wikimedia.org/wiki/File:Metal_movable_type.jpg), „Metal movable type“, https://creativecommons.org/licenses/by-sa/3.0/legalcode
''
### la typographie comme grille 

> “Typography: the word itself implies the concept of standardization. Without Gutenberg’s invention of the adjustable type mould it would be impossible to cast letters of different widths to exactly the same standardized body size; without standard proportions of spacing material and standard typecase or matrix-case lays, composition would be chaotic; without standardization of body sizes it would be, at the least, excessively tedious to use types from different foundries; without standardization of paper sizes, machines would run to capacity on some jobs, to economic disadvantage on others.”

> “A proper case for complete standardization cannot be made. People, written languages, culture clearly differ. The letterform which suits one language may not suit another, with different letter-frequencies and combination of digrams and trigrams: more, the exact size of the type (in the metal) may not be precisely that which, given free choice and no restrictions to point and half-points units, the typographer might juge best”.
> 
> – Anthony Froshaug, Typographic norms (1964)

{{:cours:koopm_f_36_02.jpg?400|}}
### dessin 

Le dessin des caractères est bien souvent le résultat de la co-influence de données historiques, culturelles et techniques.

### Ductus: geste et outil 

La forme de la lettre découle du geste et de l’outil (lettrage).

{{:cours:2nda2gqqyf5l.png?400|}}

''Gerrit Noordzij''

### la typographie comme sculpture 

Depuis Gutenberg et la typographie au plomb, la lettre peut être perçue comme une sculpture. Gutenberg est à l’origine joaillier.

Le typographe Dwiggins assume cette nature est prend ses distances des formes historiques issues du lettrage.

{{:cours:wad-m-formula_1_figa-b-1500.jpg?400|}}

{{:cours:wad-m-formula_7_figf-3-1500.jpg?400|}}

''Dwiggins''

### pourquoi différents dessins? 

#### technique 

Pour des contraintes techniques:

  * impression sur papier de mauvaise qualité.
  * support/outil (marbre)
  * impact visuel (titres)
  * etc.

Deux exemples de corrections optiques:

{{:cours:bodoni.gif?400|}}

''Bodoni''

{{:cours:inktraps-m.gif?400|}}

''ink traps sur la Bell Centenial''

#### usage 

Fonte de titrage condensée pour maximiser l’impact des titres et hiérarchiser l’info:

{{:cours:nytimes.jpeg?400|}}

### anatomie 

anatomie de la lettre: construction, constraste.

{{:cours:b42-10-ledetailentypographie-7-1024x683.jpg?400|}}

''Le détail en typographie, Jost Hochuli, éditions B42''
### familles et classifications 

  * Rapide histoire de la typo
  * évolution de la typographie: Chapitre 11 fontes et codages
  * classification Vox/Atypi

### Sélection de typographes du XXe s. 

  * George Auriol
  * Paul Behrens
  * Morris Fuler Benton
  * [[https://indexgrafik.fr/paul-renner/|Paul Renner]]
  * Eric Gill
  * …

## Composition 

### mots 

### lignes et paragraphes 

un moteur de composition est un composant logiciel qui à pour but d’agencer spatialement les éléments d’une mise en page. Une des tâches les plus basiques d’un moteur de composition de texte est de découper un les éléments d’un texte (caractères, mots, ponctuations etc.) pour composer des lignes.

Cette simple définition pose déjà une infinité de questions:

  * où doit on couper le texte?
  * selon quels critères?
  * comment répartir les composant d’une ligne?
  * etc.

Ces question soulèvent des préoccupations d’ordre esthétiques (rythme, gris, etc.), culturelles et grammaticales, fonctionnelles (lisibilité) voir économiques (gain de place ou gain financier). Les algorithmes qui implémentent des réponses reflètent ces préoccupations.

### Différentes variables d’ajustement 

Utiliser des glyphes de taille variable. condenser, espacer les glyphes.

Gutenberg: imiter les scribes. Limitation techniques liés à la nature du caractère mobile. Invention de ligatures.

[http:%%//%%www.eyemagazine.com/blog/post/type-tuesday3image La solution d’Eric Gill]

### Compositions non-orthogonales 

{{:cours:apollinaire-il-pleut-1916.jpg?400|}}

''Guillaume Apollinaire, ‘’Il Pleut’’ (1918)''

### Composition ligne par ligne 

Les lignes sont coupées les unes après les autres

  * [[https://www.youtube.com/watch?v=Eq4i6-z3RNg|Linotype]]

{{:cours:capture_d_ecran_de_2014-09-18_13-26-53.png?400|}}

''Ligatures de Gutenberg''

{{:cours:5631375490_ca77afb329.jpg?400|}}

''La solution d'Eric Gill''
### Composition par paragraphe 

Les lignes sont coupées à l’endroit le plus approprié en prenant en compte l’ensemble du paragraphe.

cf:

  * http://en.wikipedia.org/wiki/Word_wrap
  * http://www.typografi.org/justering/gut_hz/karow.pdf
  * http://www.bramstein.com/projects/typeset/

{{:cours:dk4orlwwaaagqog.jpeg?400|}}

''Wolfang weingart''
### couleur 

{{:cours:couleur.gif?400|}}

### rythme 

#### approches 

justification

#### alignements 

cesures

### contraste 

## Page 

ligne de base, espace

### espace composite 


Gestalt theorie

{{:cours:gestalt_principles_composition.jpg?400|}}

paratexte: gerard genette

### En pratique 

choix du format: fermer des pistes

contraintes techniques:

  * imprimante
  * coût
  * images à reproduire
  * volumes du contenu

effet de citation/connotation

  * classicisme
  * littéraire
  * machine
  * etc.

usage envisagé:

  * manipulation
  * beau livre
  * livre de poche
  * etc.

{{:cours:prix_baudouin.jpg?400|}}
## Objet 

  * reliures dos carré collé
  * cousu/collé
  * broché
  * piqûre à cheval
  * piqûre à plat
  * spirales
  * réglettes plastiques

Il faut faire des maquettes en blanc.

La couverture

### séquence 

## Impression 

Le livret -> signature = uniformité de l’encrage, sens de la fibre, façonnage: pli/rainage/tracage, assemblage, massicotage

repérage

L’impression:

  * offset
  * typo
  * riso
  * techniques composites

dorure à chaud, embossage, vernis, encartage, etc.

## Exercice 

maquette en blanc experimentations typographiques carte de visite texte long: - note d’intention - bibliographie - riso

## Ressources 

  * https:%%//%%wiki.scribus.net/canvas/Page_principale
  * https:%%//%%fr.flossmanuals.net/scribus/introduction/


----
