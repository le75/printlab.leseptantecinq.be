---
categories:
    - brief
---

# Multiple 3D mix ! 

Images Plurielles, cours de sérigraphie, professeurs : F. Deltenre et A. Leray

Impression d’une (ou plusieurs) image(s) en un nombre conséquent d’exemplaires. Pliage, mise en espace et intervention  vidéo.

* Format : libre, mais gare à la mise en pratique
* Technique : sérigraphie et image numérique
* Tirage : minimum 30 exemplaires (réussis !) identiques 

## Critères d'évaluation 

* sur la relation entre le nombre d’exemplaires et l’image produite
* sur le lien avec l’image/les images projetée(s) et la mise en espace
* sur la qualité des images (jeu des couleurs, qualité plastique, finesse, adéquation avec la technique, etc.)
* sur la réalisation, la netteté, la propreté, le repérage…

## Organisation du travail (paliers) 

* pour le 2/10/2019 : premières idées, recherche de documentation
* pour le 9/10/2019 : approfondissement des recherches
* pour le 23/10/2019 : choix des meilleurs résultats et affinage 
* pour le 6/11/2019 : réalisation d’une maquette et essais de projection
* pour le 13/11/2019 : réalisation des films et impression
* pour le 20/11/2019 : impression
* pour le 27/11/2019 : impression et mise en place 
* le 4/12/2019 : remise

## Ressources 

{{youtube>Gv5sVSfDqoU}}

* [[https://developer.mozilla.org/fr/docs/Web/CSS/animation]]
* [[http://romainmarula.fr/documentation/dokuwiki/doku.php?id=exemples]]
* {{ :cours:exemples-animations-css.zip |}}
* [[https://atom.io/|éditeur de texte Atom]]
