---
title: Paysage imaginaire
summary: Photogravure sur métal
authors:
    - Alex Leray
date: 2022-09-20
---

<!-- ![](/media/morandi-composition.jpg "Composition de Giorgio Morandi") -->

## description

1. image paysage avec figure
2. bouquin de la biblio
3. scanner pour du 24x36 dans du a4 600 dpi
4. réveler 30 à 40% de l'image avec une brosse

## thème

«Contrôle/non-contrôle»

## critères d’évaluation

  * respect des briefs et bonne gestion du temps
  * qualité et quantité du travail fourni
  * engagement personnel dans les activités.
  * maîtrise et compréhension des techniques
  * adéquation des choix techniques et plastiques
  * cohérence des images et du propos développé
  * capacité de remise en question
  * qualité de la réflexion sur son travail (documentation, discussions)

## Remise

Décembre 2020


## Cours

### Cours 1 20 septembre 2022

Présentation du brief et discussion autour des images et de leur potentiel

### Cours 2 4 octobre 2022

Amener différents types de styles et de papier cartes-postale. Ce cours sera consacré à l'exploration des machines et de leur différents réglages.
