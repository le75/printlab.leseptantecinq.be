---
title: Cartes postales
summary: Réinterpréter une série de 5 cartes postales à l’aide de trames hachurées.
authors:
    - Alex Leray
date: 2022-09-20
---

![](/img/morandi-composition.jpg "Composition de Giorgio Morandi")

## description

Réinterpréter une série de 5 cartes postales à l’aide de trames hachurées.

Vous utiliserez pour ça le logiciel Inkscape et l'extension [Eggbot](/logiciels/eggbot).

Possibilité de varier les distances et les angles. 2 couleurs maximum. Impression au [Plotter](/materiel/plotter_hp_7550a).

En lien avec le travail de boites de Muriel et celui de réinterprétation de Roberta.

## thème

«Contrôle/non-contrôle»

## critères d’évaluation

  * respect des briefs et bonne gestion du temps
  * qualité et quantité du travail fourni
  * engagement personnel dans les activités.
  * maîtrise et compréhension des techniques
  * adéquation des choix techniques et plastiques
  * cohérence des images et du propos développé
  * capacité de remise en question
  * qualité de la réflexion sur son travail (documentation, discussions)

## Remise

Décembre 2020


## Cours

### Cours 1 20 septembre 2022

Présentation du brief et discussion autour des images et de leur potentiel

### Cours 2 4 octobre 2022

Amener différents types de styles et de papier cartes-postale. Ce cours sera consacré à l'exploration des machines et de leur différents réglages.
