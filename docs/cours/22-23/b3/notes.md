# Création d'un objet éditorial

En amont de la conception de votre livret accompagnement le TFE, nous explorerons à travers la création de spécimens constitutant une base de données quelques aspects d'un travail éditorial.

Ce travail de spécimens s'articulera autour de chapitres et s'appuira sur sur les fonds de la collect et de la bibliothèque.

Il vous sera demandé lors de chaque lecture (toutes les 6 semaines environs) de présenter à l'ensemble des profs l'avancement de votre travail.



## Références

- The Trade of the Teacher, par Mieke Bal
- Les plus beaux livres suisse 2015
- Notes on Book Design, par Derek Birdsall
- Dear Lulu, par Jeames Goggin
- Just in Time, or a Short History of Production, Xavier Antin, 2010
- …

## Étapes

### Rédaction

* Définir la terminologie/Circonscrire
* Dégager une problématique
* Créer un plan
* Développer un texte
* Corriger son texte

### Mise en forme/paratexte

* Citer/référencer
    * 

### Organisation

* Organiser ses fichiers
* Nommer ses fichiers
* Versionner/augmenter

### Composition

* Choisir une typo
    * Effet de citation 
    * couverture glyphique et linguistique
    * couverture stylistique
    * caractéristiques techniques
    * Licences
* Hierachiser
    * par l'espace
    * par des attributs visuels
* Gabarit
* organiser des images

### Production

* Choisir une technique d'impression
    * contraintes
        * formats, marges techniques et façonnage 
    * budget 
    * connaissance préalable et facilité de mise en oeuvre


* calpinage
* imposition


## Exercices

### Cartographier le processus

Que signifie concevoir un objet éditorial? Quelles sont les différentes étape de sa conception à sa distribution?

1. par groupe de 3 personnes, cartographier les différentes étapes (10 minutes);
2. présenter et discuter en groupe vos cartes; identifier des groupes d'étapes;
3. distribuer ces groupes d'étapes et détailler ces étapes, et lister des questions qui peuvent se poser à chaque étape.
4. trouver un format pour assembler toutes ces étapes

### Citer

Un texte est une conversation, et il convient de mettre en lumière celle-ci afin de nourrir son propos. Il convient donc d'identifier les citations et de trouver un système qui montre les relations.

1. Piocher dans sa bibliothèque, celle de l'école ou la collec 3 systèmes de référencement au sens large: référence, cartel, index, cartographie, exergue…
2. installer Zotero
3. Maintenir une collection d'images sourcées, avec un système de classification et/ou d'annotation.


### Format

- imprimer la surface maximale de chaque imprimante


### Comparer les différentes qualités d'impression

1. lister toutes les possibilités d'impression, à l'école (numérique ou non) et en dehors (copyshop, service en ligne…)
2. créer la fiche signalétique de la technique

- faire des samples avec différents papiers (meilleurs livres suisses)
- un fichier qu'ils impriment sur différents papiers avec différentes qualités
- même objet imprimé en plusieurs fois avec plusieurs qualités
- fichier image reproduit dans plusieurs résolutions / trames

### Répertoire typographique

1. Piocher dans sa bibliothèque ou celle de l'école 5 pages variées, dont la composition et le dessin typographique nous semble intéressant (texte de labeur, titraille, paratexte…)
2. Commenter pour chaque typographie ses caractéristiques: graisse, chasse, contraste, lettres particulières… et son contexte d'utilisation.
3. mettre en commun, regrouper par famille et nommer ses familles. Annoter leurs caractéristiques.
4. chercher parmis les typothèques des familles de caractères libres, ayant les même caractéristiques
5. créer un plié (montage au copieur) pour chaque famille de caractère présentant la famille typographique


### Reader

Un reader est une compilation de textes traitant d'un même sujet.

1. choisir un texte, le lire et relever 2-3 idées importantes
2. la semaine suivante, en faire une synthèse à la classe
3. Selectioner un extrait de ce texte
3. remettre en page ce texte sur une page

#### Liste de textes

- Texte de Bruno sur l'imprimé
- The M formula
- Le geste d'écrire, Vilém Flusser
- Unjustified text, Robin Kinross
- Typography is a grid
- On Choosing a Typeface, Derek Birdsall

## Mots-clés

- séquence
- format
- pli
- …

## Notes

- format, pli, séquence
une seule image / deux images qui se déplient
À partir de leur collection d'images? À partir d'un travail à eux?
- pre-imprimer des cartes à remplir
- cookbooks (etherpad-style?)
- système de bibliothèque et de cross-reference (inclure les samples de Muriel et des objets trouvés et la collec)
- trouver un système pour assembler les choses (boites, rangements, etc.)
- Chapitres:
    - citations (au sens large: inclus cartes/cross-references, etc.)
    - Styles
    - reglages typo


