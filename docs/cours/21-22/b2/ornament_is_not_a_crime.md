---
categories:
    - brief
---

# «Ornament is not a crime» 

À partir des notions techniques abordées en cours, créér à l'aide d'Inkscape un
ou des motif répétitifs (raccords invisibles). Ces motifs devront ensuite être
utilisés pour créer une oeuvre en (très) grand format, dans la technique de
votre choix (plotter, riso, mais aussi sérigraphie, bois, lino, etc.)

L'ensemble des fichiers finaux devront être compilés, remis, documentés.

## Points d'attention 

* Utiliser le site nuage.leseptantecinq.be pour sauvegarder vos fichiers

## Critères de notation 

* respect des contraintes (grand format, motif répétitif…)
* qualité de composition;
* documentation;
* participation;
* soin (organisation des fichiers…).

## Rendu 

À préciser.

## ressources 

* *Ornement et Crime* <https://fr.wikipedia.org/wiki/Ornement_et_crime>
* Groupe de papier peint <https://fr.wikipedia.org/wiki/Groupe_de_papier_peint>
* Organisation de surface <http://culturesnumeriques.erg.be/IMG/pdf/organisation_de_surface.pdf>
* Pattern design after William Morris <https://www.vam.ac.uk/articles/pattern-design-after-william-morris>
* Dries Van Noten <https://www.driesvannoten.com/looks/Women--S-S--2021>
* Inspiration <https://www.pinterest.fr/ali_ray/motifs/>
* Floss Manual <https://fr.flossmanuals.net/inkscape/tile-clone/>
