---
categories:
    - brief
---

# Ornament is NOT a Crime

## description 

Créer une série de motifs répétitifs.

Vous utiliserez pour ça le logiciel [[ressources:inkscape]].

En lien avec le travail de boites de Muriel et celui de Roberta.



{{:cours:20-21:b2:125_whoswho_003_sito.jpg?300|}}
{{:cours:20-21:b2:125_whoswho_023.jpg?300|}}
{{:cours:20-21:b2:125_whoswho_020_sito.jpg?300|}}
{{:cours:20-21:b2:125_whoswho_004_mailchimp.jpg?300|}}
{{:cours:20-21:b2:94_gianpaolopagnibiologia1.jpg?600|}}
{{:cours:20-21:b2:94_gianpaolopagnibiologia3.jpg?600|}}
{{:cours:20-21:b2:94_gianpaolopagnibiologia4.jpg?600|}}
{{:cours:20-21:b2:109_008ddmcpagni.jpg?600|}}
{{:cours:20-21:b2:109_001ddmcpagni.jpg?600|}}

## thème 

“Interieur/Extérieur”

## critères d’évaluation 

  * respect des briefs et bonne gestion du temps
  * qualité et quantité du travail fourni
  * engagement personnel dans les activités.
  * maîtrise et compréhension des techniques
  * adéquation des choix techniques et plastiques
  * cohérence des images et du propos développé
  * capacité de remise en question
  * qualité de la réflexion sur son travail (documentation, discussions)

## Remise 

Le xxx

## Ressources 

  * [[http://culturesnumeriques.erg.be/IMG/pdf/organisation_de_surface.pdf| Organisation de Surface. Erg: Culture Numérique]]

