---
categories:
    - brief
    - workshop
---

# Flipbooks 

![](/img/munari_moire.jpg "Bruno Munari")

## Descriptif 

Le moiré résulte de l'interférence de deux motifs réguliers superposés. Durant un jour et demi, nous allons explorer la crátion de moirés et créer un petit flipbook.

Nous explorerons les choses suivantes:

* Création de trames vectorielles à l'aide du logiciel Inkscape, ou bien à travers des moyens alternatifs (agrandissement d'une trame riso ou de vieux journaux, papier millimétré, perforatrice… on verra ce qui marche!)
* Impression de trames avec le traceurs à stylos sur Rhodoïd
* Utilisation directe des copieurs ou riso pour varier les trames
* Impression d'une séquence et reliure sous forme de flipbook.

