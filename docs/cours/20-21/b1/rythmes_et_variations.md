---
categories:
    - brief
---


# Rythmes et variations 

![Steve Reich, pochette d'album de Music for 18 Musicians](/img/reich.jpg "Steve Reich, pochette d'album de Music for 18 Musicians")

## Descriptif 

Créer à l’aide du langage de programmation [Logo](/logiciels/logo), une série de quatre impressions en deux passages d’après le travail de [](..:..:Steve Reich).

Il ne s’agit pas nécessairement de traduire une musique spécifique (mais ça peut), mais votre travail doit mettre en œuvre certains des principes de compositions de Reich, parmi lesquels:

  * répétition d’un motif, rythme;
  * phasage/déphasage;
  * minimalisme;
  * variations;
  * permutations.

**Ce travail ne peut pas être figuratif. Il doit être abstrait, géométrique et minimal.** Il ne s’agit pas de faire compliqué: l’exploration des paramètres d’impression est tout aussi voir plus importante!

En plus de tenir un journal de bord avec vos étapes de travail, l’ensemble des fichiers finaux devront être compilés, remis, documentés.

## Critères de notation 

  * qualité de composition;
  * recherche plastique dans l’impression;
  * cohérence de la série;
  * documentation;
  * participation;
  * soin (organisation des fichiers…).

## Rendu 

Jeudi 16 novembre (lectures module 1).

## Planning 

### vendredi 18 septembre, 9h-17h 

  * Présentation du cours et du Printlab;
  * Présentation de pratiques numériques;
  * Présentation du sujet;
  * Introduction au dessin par instructions;
  * Exercices: concevoir des compos sur papier.

### mercredi 30 septembre, 8h30-12h30 

  * Introduction à [Logo](/logiciels/logo);
  * Exercices:
    * créer un projet sur papier;
    * créer les formes de base.

### mercredi 7 octobre, 8h30-12h30 

  * Perfectionnement à Logo;
  * Exercices:
    * Réaliser le projet sur papier à l’aide de grilles;

### mercredi 20 octobre, 8h30-12h30 

  * Introduction au Plotter;
  * Premières impressions.

### mercredi 28 octobre, 8h30-12h30 

  * Formation gravure Laser Fablab.

### mercredi 4 novembre, 8h30-12h30 

  * Finalisation des fichiers pour impression.

### mercredi 10 novembre, 8h30-12h30 

  * Gravure Laser au Fablab.

### mercredi 17 novembre, 8h30-12h30 

  * Impression dans l’atelier Gravure.

### jeudi 19 novembre 

  * rendu module 1.

## Ressources 

  * [Exemples](https://www.pinterest.com/ali_ray/)
  * [Pad pour impression](http://annuel.framapad.org/p/rythmes_et_variations)
