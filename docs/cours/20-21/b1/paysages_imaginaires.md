---
categories:
    - brief
---

# Paysages imaginaires 

## Descriptif 

Créer à l’aide du logiciel [Gimp](/logiciels/gimp), des trames fournies et des techniques de découpage numérique abordées en cours une composition représentant un paysage imaginaire.

Ce travail, au format 15x10 cm sera ensuite reporté par insolation sur métal à l'aide de la technique du film sec, et imprimée dans le cours de Valérie Rouiller.

Vous tiendrez un journal de bord avec vos notes et étapes de travail. L’ensemble des fichiers finaux devra être compilé, remis, documenté.

## Critères de notation 

* qualité de composition;
* utilisation appropriée des trames;
* recherche plastique dans l’impression;
* qualité technique de la réalisation
* documentation;
* participation;
* soin (organisation des fichiers…).

## Rendu 

Module 2 gravure.

## Planning 

À venir

## Ressources 

* [Textures](/img/textures_out.zip)
