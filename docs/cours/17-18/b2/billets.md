---
categories:
    - brief 
---

# Billets

## contexte 

> «Une image binaire est une image numérique dont chaque pixel ne peut être que de deux valeurs. Typiquement, les deux couleurs utilisées pour les images binaires sont le noir et le blanc»
> 
> Wikipédia, traduction libre

Le procédé le plus simple pour créer une image binaire consiste à mesurer la luminosité de chaque pixel et, en fonction d'un seuil donné (typiquement 50%), décider si ce pixel sera blanc ou noir. Cette méthode a cependant l'inconvénient d’éliminer les nuances de l'image traitée.

Pour pallier à cette limitation, de nombreuses techniques de tramage ont été élaborées au travers de l'histoire de l'image numérique. Que ce soit pour des raisons techniques (ex. pouvoir transmettre une image fidèle et légère par télécopieur) ou esthétiques (ex. obtenir un effet pointilliste), artistes et ingénieurs ont élaborés des méthodes permettant de restituer visuellement un effet de valeur entre le blanc et le noir. Chacune utilisent des stratégies différentes mais toutes ont en commun de prendre en compte les pixels environnants.

À travers ce module, nous explorerons quelques-unes de ces méthodes comme le *dithering* (ajout de bruit contrôlé dans l'image pour rendre plus fidèlement les nuances de gris), le *stippling* (pointillisme) ou encore diverses manières numériques de simulation de gravure.

## Travail à fournir 

Créer et imprimer un billet ou une série de billets d'une monnaie fictive que vous inventerez. Ces billets doivent reprendre les conventions fiduciaires d'usage et faire figurer en leur recto un portrait et en leur verso un paysage. Ils doivent utiliser au moins deux techniques numériques de tramage. En imprimer une liasse.

## Critères de notation 

  * participation
  * documentation
  * qualité de trames
  * qualité de composition
  * adéquation des techniques d'impression mises en oeuvre
  * qualité d'impression

## Rendu 

Mercredi 22 novembre
