---
categories:
    - brief 
---

# T,O,U,C,H,I,N,G

<!-- {{tag>images-plurielles B1}} -->



[![Richard Sharits, T,O,U,C,H,I,N,G](/img/sharfeat.jpg "Paul Sharits, T,O,U,C,H,I,N,G, 1968")](https://yewtu.be/watch?v=wzRAB2MjSNA)

## Paul Sharits 

[Paul Sharits](https://fr.wikipedia.org/wiki/Paul_Sharits) est un cinéaste expérimental américain, lié à [Fluxus](https://fr.wikipedia.org/wiki/Fluxus) et au [Cinéma structurel](https://fr.wikipedia.org/wiki/Cin%C3%A9ma_structurel). Ses vidéos intéressent donc davantage à la structure qu’au contenu. Il se donne des contraintes, des règles formelles – variations, rythmes etc. – pour créer chez le spectateur des impressions sensorielles.

## Imagemagick 

Imagemagick est un ensemble de logiciels de traitement d’image, qui ne possèdent pas d’interface graphique mais s’utilisent à l’aide du terminal. Ces logiciels ne sont pas issue de la culture du graphisme ou de l’art, mais plutôt de l’ingénierie.

L’un de ces programmes s’appelle `convert`. Initialement pensé pour convertir une image d’un format vers un autre, il permet aussi d’appliquer une série de traitements et filtres. Une fois la recette trouvée, on peut ainsi l’appliquer systématiquement sur un grand nombre d’images.

## Travail à fournir 

À la manière de Paul Sharrits et à l’aide du logiciel `convert`, réaliser une série de 48 images variations d’une images qui, par leur succession, créerons une impression particulière chew le spectateur. Faire un GIF animé et imprimer une planche contact de l’ensemble.

L’ensemble des commandes doit-être enregistré dans un fichier texte, et documenté. À partir de l’image d’origine et des différentes lignes de code créées on doit pouvoir reproduire votre série. Ce travail est aussi important que le résultat final et sera pris en compte dans l’évaluation finale.

## Critères de notation 

* participation
* documentation, reproductibilité
* qualités plastiques: rythmes, structure.

## Rendu 

mardi 27 mars, 12h30
