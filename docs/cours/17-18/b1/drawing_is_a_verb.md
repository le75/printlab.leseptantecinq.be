---
categories:
    - brief 
---

# Drawing is a Verb


![Richard Serra's Verblist](/img/serra_verb_list.jpg "Richard Serra's Verblist")

Lors de ce module, nous nous intéresserons à travers le dessin à la question plastique et numérique essentielle qui est l’instruction.

De l’art conceptuel à l’art vidéo en passant par l’architecture, de nombreux artistes, en particulier au cours de la deuxième moitié du XX<sup>e</sup> siècle se sont intéressés à la question du langage. Tout comme l’écriture, le numérique est un système *mnémo-technique* (technique de mémorisation) qui permet de délinéariser, de mémoriser et de reproduire des flux: musique, films mais aussi dessin.

Cette reproductibilité change radicalement la nature d’un oeuvre qui n’est dès lors plus liée à un moment donné dans le temps et l’espace, mais peut être rejoué à l’infini. Délinéariser suppose de définir une méthode, et *in fine* un langage qui, par son vocabulaire, exprimera avec plus ou moins de nuances les contours d’une pratique.

## Travail à fournir

À partir des différentes expérimentations réalisées en groupe ou de vos propres apports, créez une série de trois œuvres basées sur un processus par instruction.

Ces œuvres ne sont pas nécessairement numérique. Elle doivent cependant mettre en place des logiques numériques.

1. La première doit pouvoir produire des résultats les plus identiques possible à chaque exécution.
2. La deuxième est fonctionnera sur le principe de la première mais introduira la notion de libre arbitre.
3. La troisième doit introduira la notion de variables (qui n’est pas synonyme ambiguïté) pour produire des résultats à chaque fois différents.

Cette oeuvre doit être exécutée au moins trois fois par trois par des personnes différentes, et communiquée uniquement par un langage notationel.

## Critères de notation 

* participation
* documentation
* reproductibilité
* expressivité de la notation
* qualités plastiques propres aux contraintes

## Rendu 

Mardi 21 novembre
