![](/img/papert-logo-bot.jpg "À gauche, Seymour Papert, créateur de Logo")

## C'est quoi Logo? 

{{youtube>bOf4EMN6-XA}}

## Quels sont les bases à connaitre? 

  * commandes de base
  * types
  * tableaux
  * variables
  * boucles
  * fonctions

## Copions 

Lancer le programe ucblogo

```bash
logo
```
Quitter le programme

```logo
bye
```
avancer de 100 pixels

```logo
forward 100
```
tourner de 45 degrés à droite

```logo
right 45    
```
Tracer un arc de 45 degré d’un cercle de rayon 50 pixels

```logo
arc 90 50
```
Lever la plume

```logo
penup
```
Baisser la plume

```logo
pendown
```
Commenter une ligne

```logo
; Ceci est un commentaire
```
Définir la couleur de fond (7 = blanc)

```logo
setbackground 7
```
Définir la couleur de la plume (0 = noir)

```logo
setpencolor 0
```
Répéter une opération (pour faire ici un carré)

```logo
repeat 4 [
    forward 100
    right 90
] 
```

Sauvegarder son programme sous le nom ''%%file.eps%%''

```logo
epspic "file.eps
```
Définir son éditeur de texte (ici ''%%nano%%'')

```logo
seteditor "nano
```

Sauvegarder son programme en ''%%mon_dessin.lg%%''

```logo
save "mon_dessin.lg
```

Lire la procédure ''%%hello%%''

```logo
po "hello
```
Définir la variable ''%%rayon%%'' à la valeur 100

```logo
make "inner 100

for [value 4 11 3] [print :value] -> de 4 à 11 par incŕement de 3

```logo
Déplacer le curseur en position absolue des variable ''%%x%%'' et ''%%y%%''

```logo
setpos (list :x :y)
```

## Documentation 

* [Manuel de Logo](https://people.eecs.berkeley.edu/~bh/v2ch14/manual.html)
* [Logo Primer](https://el.media.mit.edu/logo-foundation/what_is_logo/logo_primer.html)

## Implementations de Logo 

* [JSlogo](https://www.calormen.com/jslogo/#)
* [ACSLogo](https://www.alancsmith.co.uk/logo/index.html), un éditeur pour Mac. Non-testé.

## Lecture

* [La géométrie tortue](http://f-u-t-u-r-e.org/r/13_Seymour-Papert_La-Geometrie-tortue-extrait_FR.md)

## Quelques exemples

* [[Lewitt 01]]
* [[Lewitt 02]]
* [[Lewitt 03]]
* [[Carrelage]]
* [[Gerstner]]
* [[Grille Simple]]
* [[nid d'abeilles]]

## Ressources

* [15 words contest](http://www.mathcats.com/gallery/15wordcontest.html)
