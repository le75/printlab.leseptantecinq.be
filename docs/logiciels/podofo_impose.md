PODOFO Impose est un logiciel en ligne de commande permettant de réarranger les pages d'un document PDF. Il est particulièrement utile pour créer des "impositions" -- des signatures et ainsi générer des livret à imprimer.

Son usage est le suivant:

```bash
for i in *.txt; do
podofoimpose pdf_list.txt 2in1poster.pdf superimpose.plan lua
done
```

  * pdf_list.txt est un fichier texte contenant une liste de documents PDF à traiter
  * 2in1poster.pdf est le nom du fichier de sortie
  * superimpose.plan est le nom du fichier contant les instructions lua pour réarranger les pages

## Superposer plusieurs documents 

```lua
-- superimpose.plan
-- source: Pierre Marchand/Prince Charmand

-- Required
PageWidth = SourceWidth
PageHeight = SourceHeight

-- Records
i = 1
while i <= PageCount
do
        -- PushRecord(sourcepage, targetpage, rotation, x, y)
        PushRecord(i , 1 , 0, 0, 0)
        i = i + 1
end
```

## Arranger plusieurs pages sur un format plus grand 

```lua
-- arrange.plan
-- A lua plan for podofo-impose that arranges pdf pages onto a bigger page
-- Similar to Imagemagick montage command

-- The size of our target document. Here: an A3 (in pt)
PageWidth = 842
PageHeight = 1191

-- Computes the number of columns and rows
cols = math.floor(PageWidth / SourceWidth)
rows = math.floor(PageHeight / SourceHeight)
itemsPerPage = cols * rows


-- Records
currentSourcePage = 1
while currentSourcePage <= PageCount
do
    -- the current column
    currentCol = (currentSourcePage - 1) % cols
    -- the current x position
    x = SourceWidth * currentCol

    -- the current row
    currentRow = math.floor((currentSourcePage - 1) / cols) % rows
    -- the current y position
    y = SourceHeight * currentRow

    -- the current page
    currentTargetPage = math.ceil(currentSourcePage / itemsPerPage)

    -- PushRecord(sourcepage, targetpage, rotation, x, y)
    PushRecord(currentSourcePage, currentTargetPage, 0, x, y)

    currentSourcePage = currentSourcePage + 1
end

-- vim:ft=lua:
```

## Arranger plusieurs pages sur un format plus grand (mise à jour) 

```lua
-- arrange.plan
-- A lua plan for podofo-impose that arranges pdf pages onto a bigger page
-- Similar to Imagemagick montage command

-- 1mm is 2.83464567pt
mm = 2.83464567

-- The size of our target document. Here: an A3
PageWidth = 297 * mm
PageHeight = 420 * mm

-- Sets the margins
MarginTop = 40
MarginRight = 40
MarginBottom = 40
MarginLeft = 40

--241 x 156
--298 x 213
--57 / 2 = 28.5 
CropBoxWidth = 298
CropBoxHeight = 213
Offset = ((CropBoxWidth - SourceWidth) / 2)

-- Effective Width and Height
AreaWidth = PageWidth - MarginRight - MarginLeft
AreaHeight = PageHeight - MarginTop - MarginBottom

-- Computes the number of columns and rows
cols = math.floor(AreaWidth / CropBoxWidth)
rows = math.floor(AreaHeight / CropBoxHeight)
itemsPerPage = cols * rows

-- Records
currentSourcePage = 1
while currentSourcePage <= PageCount
do
    -- the current column
    currentCol = (currentSourcePage - 1) % cols
    -- the current x position
    x = MarginLeft + (CropBoxWidth * currentCol) + Offset

    -- the current row
    currentRow = math.floor((currentSourcePage - 1) / cols) % rows
    -- the current y position
    y = MarginTop + (CropBoxHeight * currentRow) + Offset

    -- the current page
    currentTargetPage = math.ceil(currentSourcePage / itemsPerPage)

    -- PushRecord(sourcepage, targetpage, rotation, x, y)
    PushRecord(currentSourcePage, currentTargetPage, 0, x, y)

    currentSourcePage = currentSourcePage + 1
end

-- vim:ft=lua:
```

## 2*16 pages encartés 

```lua
-- Required
PageWidth = SourceWidth * 4
PageHeight = SourceHeight * 2

-- PushRecord(sourcepage, targetpage, rotation, x, y)
PushRecord(32, 1, 0, 0, 0)
PushRecord(1, 1, 0, SourceWidth, 0)
PushRecord(4, 1, 0, SourceWidth * 2, 0)
PushRecord(29, 1, 0, SourceWidth * 3, 0)

PushRecord(25, 1, 180, SourceWidth, SourceHeight * 2)
PushRecord( 8, 1, 180, SourceWidth * 2, SourceHeight * 2)
PushRecord( 5, 1, 180, SourceWidth * 3, SourceHeight * 2)
PushRecord(28, 1, 180, SourceWidth * 4, SourceHeight * 2)


PushRecord(26, 2, 0, 0, 0)
PushRecord( 7, 2, 0, SourceWidth, 0)
PushRecord( 6, 2, 0, SourceWidth * 2, 0)
PushRecord(27, 2, 0, SourceWidth * 3, 0)

PushRecord(31, 2, 180, SourceWidth, SourceHeight * 2)
PushRecord( 2, 2, 180, SourceWidth * 2, SourceHeight * 2)
PushRecord( 3, 2, 180, SourceWidth * 3, SourceHeight * 2)
PushRecord(30, 2, 180, SourceWidth * 4, SourceHeight * 2)





PushRecord(24, 3, 0, 0, 0)
PushRecord( 9, 3, 0, SourceWidth, 0)
PushRecord(12, 3, 0, SourceWidth * 2, 0)
PushRecord(21, 3, 0, SourceWidth * 3, 0)

PushRecord(17, 3, 180, SourceWidth, SourceHeight * 2)
PushRecord(16, 3, 180, SourceWidth * 2, SourceHeight * 2)
PushRecord(13, 3, 180, SourceWidth * 3, SourceHeight * 2)
PushRecord(20, 3, 180, SourceWidth * 4, SourceHeight * 2)


PushRecord(18, 4, 0, 0, 0)
PushRecord(15, 4, 0, SourceWidth, 0)
PushRecord(14, 4, 0, SourceWidth * 2, 0)
PushRecord(19, 4, 0, SourceWidth * 3, 0)

PushRecord(23, 4, 180, SourceWidth, SourceHeight * 2)
PushRecord(10, 4, 180, SourceWidth * 2, SourceHeight * 2)
PushRecord(11, 4, 180, SourceWidth * 3, SourceHeight * 2)
PushRecord(22, 4, 180, SourceWidth * 4, SourceHeight * 2)



-- vim:ft=lua:
```
