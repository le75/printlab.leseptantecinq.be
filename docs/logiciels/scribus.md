![interface de Scribus](/img/scribus.png "Interface de Scribus")

Scribus est un logiciel de mise en page pour l'imprimé. Il permet de créer des fichiers PDF de qualité professionnelle, pour une impression en offset. Il est gratuitement, sous licence libre GPLv2.

* [Site officiel](https://www.scribus.net/)
