---
tags:
  - vidéo
  - ligne de commande
categories:
    - logiciel
    - recette
---

# FFmpeg


## Régler les niveaux d'une vidéo

```bash
ffmpeg -i input.mov -filter_complex "colorlevels=rimin=0.19:gimin=0.29:bimin=0.24:rimax=0.76:gimax=0.91:bimax=0.87" output.mov
```

## Combiner deux vidéos

```bash
ffmpeg -i contrast.mp4 -i IMG_2918.mov -filter_complex "[0:v] format=rgba [bg]; [1:v] format=rgba [fg]; [bg][fg] blend=all_mode='multiply':all_opacity=1, format=rgba"  combined.mp4
```

## Avec décalage de la deuxieme video

```bash
ffmpeg -i contrast.mp4 -itsoffset 3 -i contrast.mp4  -filter_complex "[0:v] format=rgba [bg]; [1:v] format=rgba [fg]; [bg][fg] blend=all_mode='multiply':all_opacity=1, format=rgba"  combined.mp4
```

## Avec de la rotation

```bash
ffmpeg -i contrast.mp4 -itsoffset 3 -i contrast.mp4  -filter_complex "[0:v] hflip,vflip,format=rgba [bg]; [1:v] format=rgba [fg]; [bg][fg] blend=all_mode='multiply':all_opacity=1, format=rgba"  combined.mp4
```


## Coloriser l'image

```bash
ffmpeg -i combined2.mp4 -i noir.png -filter_complex "[0:v] format=rgba [bg]; [1:v] format=rgba [fg]; [bg][fg] blend=all_mode='screen':all_opacity=1, format=rgba"  combined_screen_noir.mp4
```

## Script complexe

```bash
ffmpeg \
    -i in/input.mov \
    -i in/rose.png \
    -i in/bleu.png \
    -i in/noir.png \
    -filter_complex \
    "[0:v] scale=720:-1 [in]; 
     [1:v] scale=720:-1 [rose]; 
     [2:v] scale=720:-1 [bleu]; 
     [3:v] scale=720:-1 [noir]; 
     
     [in] colorlevels=rimin=0.19:gimin=0.29:bimin=0.24:rimax=0.76:gimax=0.91:bimax=0.87 [in]; 
     
     [in] split=4 [a][b][c][d];
     [b] hflip,setpts=PTS+1/TB [b];
     [c] vflip,setpts=PTS+2/TB [c];
     [d] hflip,vflip,setpts=PTS+3/TB [d];
     
     [a][b] blend=all_mode='multiply':all_opacity=1 [ab]; 
     [c][d] blend=all_mode='multiply':all_opacity=1 [cd];
     [ab][cd] blend=all_mode='multiply':all_opacity=1 [abcd];
     
     [abcd] split=3 [abcd_1][abcd_2][abcd_3];
     [abcd_1][rose] blend=all_mode='screen':all_opacity=1 [abcd_rose];
     [abcd_2][bleu] blend=all_mode='screen':all_opacity=1 [abcd_bleu];
     [abcd_3][noir] blend=all_mode='screen':all_opacity=1 [abcd_noir];
     [abcd_bleu] vflip,setpts=PTS+0.3/TB [abcd_bleu];
     [abcd_noir] hflip,setpts=PTS+0.6/TB [abcd_noir];
     
     [abcd_rose][abcd_bleu] blend=all_mode='multiply':all_opacity=1 [abcd_rose_bleu];
     [abcd_rose_bleu][abcd_noir] blend=all_mode='multiply':all_opacity=1,format=rgba [abcd_rose_bleu_noir];

     [abcd_rose_bleu_noir] split=2 [seq1][seq2];
     [seq2] vflip,hflip,setpts=PTS+3.7/TB [seq2];
     [seq1][seq2] blend=all_mode='multiply':all_opacity=1,format=rgba" \
    test.mp4
```
