![](https://media.inkscape.org/media/resources/file/Align-on_canvas.gif "Aperçu de la fonctionalité Aligner")

Inkscape est un logiciel de dessin vectoriel Libre/Open Source et gratuit, développé par une communauté dynamique.

Depuis la version 1.0, Inkscape est disponible en natif sur Mac Os. Il fonctionne donc parfaitement sur cette plateforme, ainsi que sur Linux et Windows.

## Liens 


* [Site officiel](https://inkscape.org/fr/)
* [Téléchargement](https://inkscape.org/fr/release/inkscape-1.0.1/)

## Voir aussi 

* [Eggbot](https://wiki.evilmadscientist.com/Installing_software)
