---
tags:
  - bitmap
  - ligne de commande
categories:
    - logiciel
    - recette
    - bitmap
---

# Imagemagick

Imagemagick est le couteau suisse du traitement d'images Bitmap. Il s'agit d'un ensemble de programmes en lignes de commande, dont le plus connu est "convert".

## convertir une série d'images en gif animé 

```bash
convert 01.gif 02.gif 03.gif animation.gif
```

Avec un délai de 25/100 de seconde

```bash
convert *.png -set delay 25 resultat.gif
```

## Exemples de commandes liées au tramage 

Quelques exemple de commande de tramage.

![](/img/imagemagick/axel_korban.jpg)

Photographie d'Axel Korban, tirée du magazine [Médor Numéro 8](https://medor.coop/magazines/medor-8-autumn-2017), CC BY-SA

## Obtenir la liste des algorithmes de diffusion dispnibles 

```bash
convert -list dither
```

* Voir https://fr.wikipedia.org/wiki/Diffusion_d%27erreur
* et https://pierremuth.wordpress.com/tag/error-diffusion/

## Obtenir la liste des motif de seuil 

```bash
convert -list threshold
```

## convertir une image en monochrome 

```bash
convert axel.jpg -monochrome out/axel_monochrome.png
```

![](/img/imagemagick/axel_monochrome.png)


## Utiliser un palette prédéfinie 

```bash
convert axel.jpg -remap pattern:gray50 out/axel_gray_50.png
```

![](/img/imagemagick/axel_gray_50.png)

Remarque: on peut spécifier l'algorithme de threshold soit même.

```bash
convert axel.jpg -dither Riemersma  -remap pattern:gray50 out/axel_gray_50_riemersma.png
```

## Passer une image en seuil 

```bash
convert axel.jpg   -threshold  50%  out/axel_threshold_50.png
```

![](/img/imagemagick/axel_threshold_50.png)

## Seuil sur les deux couleurs principales 

```bash
convert  axel.jpg  +dither  -colors 2  -colorspace gray -normalize out/axel_threshold_two_colors.gif
```

![](/img/imagemagick/axel_threshold_two_colors.gif)

## Trame aléatoire en couleur 

```bash
convert  axel.jpg  -channel All -random-threshold 0x100% out/axel_random_threshold_all_channels.gif
```

![](/img/imagemagick/axel_random_threshold_all_channels.gif)

12 images mises bout à bout:

![](/img/imagemagick/axel_random_threshold_all_channels_animated.gif)

## Trame aléatoire en noir et blanc 

```bash
convert  axel.jpg -colorspace gray -random-threshold 0x100% out/random_threshold_gray.gif
```

![](/img/imagemagick/random_threshold_gray.gif)

## Motif géométrique o2x2 

```bash
convert axel.jpg -colorspace Gray -ordered-dither o2x2 out/axel_ordered_dither_2_2.png
```

![](/img/imagemagick/axel_ordered_dither_2_2.png)

## Motif géométrique o8x8 

```bash
convert axel.jpg -colorspace Gray -ordered-dither o8x8 out/axel_order_dither_ordered_8_8.png
```

![](/img/imagemagick/axel_order_dither_ordered_8_8.png)

## Seuil 

```bash
convert axel.jpg -colorspace Gray -ordered-dither threshold out/axel_ordered_dither_threshold.png
```

![](/img/imagemagick/axel_ordered_dither_threshold.png)

## Motif géométrique damier 

```bash
convert axel.jpg -colorspace Gray -ordered-dither checks out/axel_ordered_dither_checks.png
```

![](/img/imagemagick/axel_ordered_dither_checks.png)

## ??? 

```bash
convert axel.jpg -colorspace Gray -ordered-dither h8x8o out/axel_ordered_dither_8_8_o.png
```

![](/img/imagemagick/axel_ordered_dither_8_8_o.png)


## Motif géométrique c7x7w 

```bash
convert axel.jpg -colorspace Gray -ordered-dither c7x7w out/axel_ordered_dither_7_7_w.png
```

![](/img/imagemagick/axel_ordered_dither_7_7_w.png)

## Motif géométrique personnalisé 

Modifier le fichier /etc/ImageMagick-7/thresholds.xml (sur Linux) et rajouter ces lignes: 

```bash
# <threshold map="hlines12x4" alias="hlines">
# <description>Horizontal Lines 12x4</description>
# <levels width="12" height="4" divisor="9">
# 7 8 8 8 8 7 6 6 5 5 5 6
# 2 1 1 1 1 2 3 4 4 4 3 3
# 6 6 5 5 5 6 7 8 8 8 8 7
# 3 4 4 4 3 3 2 1 1 1 1 2
# </levels>
# </threshold>
```


```bash
convert axel.jpg -colorspace Gray -ordered-dither hlines12x4 out/axel_ordered_dither_hlines_12_4.png
```

![](/img/imagemagick/axel_ordered_dither_hlines12x4.png)


## Ressources supplémentaires 

  * [Dithering experiments](http://www.simplesystems.org/users/glennrp/dithering/)
  * [Manuel d'imagemagick (section "Color Quantization and Dithering")](http://www.imagemagick.org/Usage/quantize/)
