# Rédaction

* Définir la terminologie/Circonscrire
* Dégager une problématique
* Créer un plan
* Développer un texte
* Corriger son texte

# Mise en forme/paratexte

* Citer

# Organisation

* Organiser ses fichiers
* Nommer ses fichiers
* Versionner/augmenter

# Composition

* Choisir une typo
    * Effet de citation 
    * couverture glyphique et linguistique
    * couverture stylistique
    * caractéristiques techniques
    * Licences
* Gabarit

# Production

* Choisir une technique d'impression
