Title:   Organiser ses fichiers
Summary: Conserver la structure de ses mise en page


![](/img/missing_image.png)

> «Pour chaque minute passée à s'organiser, une heure est gagnée.» - Anonyme

Une mise en page intègre différentes ressources (images vectorielles ou bitmap, polices…), généralement stockées sous forme de fichiers sur le disque-dur de l'ordinateur utilisé. Le fichier de mise en page conserve la liste et l'emplacement des ressources utilisées.

DANS CE CONTEXTE

Si une ou plusieurs ressources sont supprimés ou déplacés, ou si le fichier de mise en page est déplacé seul, les liens seront rompus et la mise en page corrompue.

MAIS

- Les dossiers permettent de regrouper de manière logique les fichiers et de les déplacer d'un bloc
- les logiciels offrent généralement une fonction d'assemblage qui permet de regrouper dans un dossier tous les éléments nécessaires.

DONC

Organiser ses fichier de façon logique

<!-- Conséquences -->
<!-- Connected patterns -->
<!-- alternative patterns -->
