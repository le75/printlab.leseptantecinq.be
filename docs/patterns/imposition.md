---
title: Imposition
summary: A brief description of my document.
authors:
    - Waylan Limberg
    - Tom Christie
date: 2018-07-10
some_url: https://example.com
---


# Imposition

## Contexte

L'imposition est l'une des étapes fondamentales du processus d'impression prépresse. Elle consiste à disposer les pages du produit imprimé sur la feuille de l'imprimante, afin d'obtenir une impression plus rapide, de simplifier la reliure et de réduire le gaspillage de papier.

## Définitions


> * Trimbox is basically the size of your page in the final product, after all cutting operations. It's the size of your page in scribus.
> * The bleedbox contains trimbox + any bleed
> * the mediabox contains bleedbox + any crop/bleed/etc... marks
> * the cropbox is mostly a viewer thing, it specified the area displayed in viewer : in Adobe Reader, the document size displayed is normally the cropbox size.
> with pdf exported by scribus the cropbox is equal to the mediabox
> 
> -- https://wiki.scribus.net/canvas/PDF_Boxes_:_mediabox,_cropbox,_bleedbox,_trimbox,_artbox


## Remove crop marks (for a PDF without correct TrimBox infos)

```js
var pdf = new PDFDocument(scriptArgs[0]);
pdf.getTrailer().Root.PageLayout = "TwoColumnLeft";
var n = pdf.countPages();
for (var i = 0; i < n; ++i) {
    var page = pdf.findPage(i);
    var currentWidth = page.MediaBox[2];
    var currentHeight = page.MediaBox[3];
    var wcrop = ((currentWidth - 482) / 2);
    var hcrop = ((currentHeight - 652) / 2);
    print(wcrop);
    page.MediaBox = page.CropBox = [wcrop,hcrop, 482 + wcrop, 652 + hcrop];
}
pdf.save(scriptArgs[1]);
```

call with `mutool run bin/resize.js source.pdf destination.pdf`

## Remove crop marks (for a PDF with correct TrimBox infos)

```js
var pdf = new PDFDocument(scriptArgs[0]);
var n = pdf.countPages();
for (var i = 0; i < n; ++i) {
    var page = pdf.findPage(i);
    page.MediaBox = page.CropBox = page.TrimBox;
}
pdf.save(scriptArgs[1]);
```

call with `mutool run bin/resize.js source.pdf destination.pdf`


## Alter Trimbox for imposition

```js
var pdf = new PDFDocument(scriptArgs[0]);
var n = pdf.countPages();
for (var i = 0; i < n; ++i) {
  var page = pdf.findPage(i);
  if (i % 2 == 1) {
    page.MediaBox = page.CropBox = [page.TrimBox[0], page.MediaBox[1], page.MediaBox[2], page.MediaBox[3]];
  } else {
    page.MediaBox = page.CropBox = [page.MediaBox[0], page.MediaBox[1], page.TrimBox[2], page.MediaBox[3]];
  }
}
pdf.save(scriptArgs[1]);
```

## Fix corrupted PDFs

```bash
gs -o repaired.pdf -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress test2.pdf
```

## 8 pages imposition plan

```lua
-- 8_pages.plan

-- Required
PageWidth = SourceWidth * 2
PageHeight = SourceHeight * 2

-- PushRecord(sourcepage, targetpage, rotation, x, y)

currentSourcePage = currentTargetPage = 1

while currentSourcePage <= PageCount
do
    i = currentSourcePage - 1

    PushRecord(i + 8, currentTargetPage, 0, 0, 0)
    PushRecord(i + 1, currentTargetPage, 0, SourceWidth, 0)
    PushRecord(i + 5, currentTargetPage, 180, SourceWidth, SourceHeight * 2)
    PushRecord(i + 4, currentTargetPage, 180, SourceWidth * 2, SourceHeight * 2)

    PushRecord(i + 6, currentTargetPage + 1, 0, 0, 0)
    PushRecord(i + 3, currentTargetPage + 1, 0, SourceWidth, 0)
    PushRecord(i + 7, currentTargetPage + 1, 180, SourceWidth, SourceHeight * 2)
    PushRecord(i + 2, currentTargetPage + 1, 180, SourceWidth * 2, SourceHeight * 2)

    currentSourcePage = currentSourcePage + 8
    currentTargetPage = currentTargetPage + 2
end

-- vim:ft=lua:
```

Call with `podofoimpose source.pdf destination.pdf 8_pages.plan lua`
