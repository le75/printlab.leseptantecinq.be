# Citer

> « Tout texte se construit comme une mosaïque de citations, tout texte est absorption et transformation d’un autre texte. » - Julia Kristeva

***

Un texte est une conversation. 

## Dans ce contexte

Il convient de mettre en lumière cette conversation

## Mais

- le texte est un bloc
- lourdeur

## Donc

identifier les citations
utiliser des conventions partagées

<!-- Conséquences -->
<!-- Connected patterns -->
<!-- alternative patterns -->
