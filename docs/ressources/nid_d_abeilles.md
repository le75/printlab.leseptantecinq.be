# Nid d'abeille

![](/img/logo/nid_abeille.png)

```logo
; détermine la taille des segments
make "len 10

; dessine trois arrêtes
to module
left 60
repeat 3 [
  forward :len
  penup back :len pendown
left 120
]
right 60
end

clearscreen

; dessine une grille avec décaage une ligne sur deux
; il y a un calcul un peu complexe avec le sinus pour trouver le décalage exact nécéssaire
for [x 0 200 (:len * sin 60) * 2] [
  make "count 0
  for [y 0 200 :len + (:len * sin 30)] [
  	penup setxy :x + ((:len * sin 60) * modulo :count 2) :y pendown
  	module
    make "count :count + 1
  ]
]

; cache la tortue
hideturtle
```
