# Typothèques

## Chasse ouverte

<https://chasse-ouverte.erg.be/>

## Use modify

<https://usemodify.com/>

## Luuse

<https://typotheque.luuse.fun/>

## Catfonts

<http://www.catfonts.de/>

## Velvetyne

<https://www.velvetyne.fr/>

## Google fonts

<https://fonts.google.com/>

## Catalogue LaTeX

<https://tug.org/FontCatalogue/allfonts.html>

## Font Library

<https://fontlibrary.org/>

## ANRT

<https://anrt-nancy.fr/fr/fonts/>

## Badass libre fonts by womxn

<https://www.design-research.be/by-womxn/>

## Open foundry

<https://open-foundry.com/>

## Typothèque Bye Bye Binary

<https://typotheque.genderfluid.space/>

## Typothèque Esa Le Septantecinq

<http://typotheque.le75.be/>

## Autres sites

- https://www.theleagueofmoveabletype.com/
- https://www.fontsquirrel.com/
- https://www.fontshare.com/
- http://phantom-foundry.com/
- https://en.wikipedia.org/wiki/Category:Open-source_typefaces
- https://www.fullautofoundry.com/
- http://www.tunera.xyz/
- https://uncut.wtf/
- https://typeof.net/Iosevka/
- http://www.cyreal.org/
- https://fonderiebretagne.fr/
- http://ospublish.constantvzw.org/foundry/
- http://copyright.rip/erg/typo/
- https://typo-inclusive.net/inventaire/
