# Gerstner

![Résultat du script](/img/logo/gerstner.png)

```logo
to rectangle :taille
repeat 4 [
  forward :taille
  right 90  
]
end
 
to grid :size :subdivisions :gutter

  ; taille d'un carreau
  make "modsize (:size - ((:subdivisions - 1) * :gutter)) / :subdivisions
  ; pas entre chaque carreau
  make "step :modsize + :gutter
 
  for [x 0 :size :step] [
    for [y 0 :size :step] [
    penup setxy :x :y pendown
      rectangle :modsize
  ]
]
end

clearscreen

setpencolor 11
grid 200 5 10
setpencolor 13
grid 200 4 10
setpencolor 12
grid 200 3 10
```
