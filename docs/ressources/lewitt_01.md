# Lewitt 01

![](/img/logo/lewitt_01.png)

```logo
make "angle 45 
make "radius 20

to quarter
penup 
back (:radius * sin :angle) left :angle
pendown 

arc (:angle * 2) :radius 

penup 
right :angle
forward (:radius * sin :angle)
pendown
end

to rquarter
make "deg pick [[0 6] [90 4] [180 1] [270 0]]
left first :deg
setpencolor last :deg
quarter
setpencolor 0
right first :deg
end

to grid
repeat 8 [
    repeat 8 [
        rquarter
        penup
        forward (sin :angle) * :radius * 2
        pendown
    ]

    penup
    back (sin :angle) * :radius * 2 * 8
    right 90
    forward (sin :angle) * :radius * 2
    left 90
    pendown
]
end


clearscreen
window
setbg 7
setpensize 3
grid

```
