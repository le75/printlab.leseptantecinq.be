# carrelage

![Résultat du script](/img/logo/carrelage.png)

``` logo
to setrelxy :x :y
    setxy (xcor + :x) (ycor + :y)
end

to shifted.old :x :y :instructions
  	make "penstate pendownp
    penup
    setrelxy :x :y
    if [:penstate] [ pendown]
    run :instructions
    penup
    setrelxy (-1 * :x) (-1 * :y)
    if [:penstate] [ pendown]
end

to shifted :x :y :instructions
  	make "penstate pendownp
    penup

    forward :y
    right 90
    forward :x
    left 90

    if [:penstate] [ pendown]
    run :instructions
    penup

    left 90 
    forward :x 
    right 90 
    back :y

    if [:penstate] [ pendown]
end


to carreau
    forward 20
    right 90
    forward 10
    right 90
    forward 20
    right 90
    forward 10
    right 90
end

to pairedecarreaux
    shifted -10 -10 [
        carreau
    ]
    shifted 0 -10 [
        carreau
    ]
end

to mur
    for [x 0 100 20] [
        for [y 0 100 20] [
            penup
            setxy :x :y
            pendown

            right pick [0 90]
            pairedecarreaux
            setheading 0
        ] 
    ]
end

clearscreen
window
setbackground 7

mur

epspict "carrelage.eps

```
