# Lewitt 03

![](/img/logo/lewitt_03.png)

```bash
to bunchoflines :count
repeat :count [
    penup
    right 90 forward 5 left 90
    pendown
    forward 235
    penup
    back 235
    pendown
]
end

clearscreen
window
setbackground 7

for [x 0 200 40] [
    penup
    setx :x
    pendown
    bunchoflines (:x / 40) + 2
]

left 90

for [x 0 200 40] [
    penup
    sety :x
    pendown
    bunchoflines (:x / 40) + 2
]

```
