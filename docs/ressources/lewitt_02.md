# Lewitt 02

![](/img/logo/lewitt_02.png)

```logo
; Dessine un rayon dont l'angle est choisi au hasard 
; dans la fourchette d'amplitude donnée
to ray :amplitude
  ; This doesn't work with UCB Logo
  ; make "angle (random 0 :amplitude)
  ; make "len (random 50 100)
  ; So I changed to this 
  make "angle random :amplitude
  make "len (random 50 + 50)
  right :angle
  forward :len
  penup
  back :len
  left :angle
  pendown
end

; Dessine plusieurs rayons
; :amplitude    -> taille des rayons
; :density      -> nombre de rayons
to bunchofray :amplitude :density
repeat :density [
    ray :amplitude
]
end

clearscreen
setbackground 7
setpencolor 1

repeat 4 [
  penup forward 200 pendown
  right 90
  bunchofray 90 20
]

setpencolor 4

repeat 4 [
    penup forward 100 pendown
    bunchofray 180 20
    penup forward 100 pendown
    right 90
]
```
