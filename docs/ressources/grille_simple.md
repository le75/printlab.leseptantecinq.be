# Grille simple

![](/img/logo/simple_grid.png)

```logo
to grid
  ; aller de 0 à 200 par pas de 50. 
  ; Enregistrer chaque valeur d'étapes dans la variable "x"
  for [x 0 200 50] [
    ; aller de 0 à 200 par pas de 50. 
    ; Enregistrer chaque valeur d'étapes dans la variable "y"
    for [y 0 200 50] [
      ; déplacer le curseur à la position "xy"
      setxy :x :y
      ; dessiner un cercle pour matérialiser la position actuelle
      arc 360 5
  ]
]
end

clearscreen
grid
```
